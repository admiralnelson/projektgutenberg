import action
import time

class TerminalUI(object):
    def __init__(self):
        self.action = action.ServerAction()

    def GenerateToken(self, time):
        self.action.GenerateKeyForPC(time)
    
    def Kick(self, pc):
        self.action.Kick(pc)

    def ListPC(self):
        return self.action.clients
    
    def ExtendTime(self, pc, time):
        self.action.Extend(pc, time)

    def UiLoop(self):
        while True:
            command = input("Masukan perintah? (? help)")
            if command == "?":
                print("tidak ada bantuan")
            if command.startswith("register"):
                commands = command.split(" ")
                if len(commands) > 2:
                    self.GenerateToken(int(commands[2]))
            if command.startswith("kick"):
                commands = command.split(" ")
                self.Kick(commands[1])
            if command.startswith("list"):
                print(self.ListPC())
            if command.startswith("extend"):
                commands = command.split(" ")
                if len(commands) > 2:
                    self.action.Extend(commands[1], commands[2])

            if command.startswith("say"):
                commands = command.split(" ")
                if len(commands) > 2:
                    self.action.SendChat(commands[1], commands[2])

x = TerminalUI()
x.UiLoop()


#