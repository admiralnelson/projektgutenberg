import json, random, string

class Database(object):
    def __init__(self):
        self.path = "db.json"
        with open(self.path, "r") as f:
            self.db = json.loads(f.read())
            pass

    def VerifyToken(self, token):
        with open(self.path, "r") as f:
            self.db = json.loads(f.read())

        res = [item for item in self.db  if item.get('token')==token]
        if(len(res) > 0): return res[0]
        return None


    def GenerateToken(self, time):
        def RandString(N):
            return ''.join(random.choices(string.ascii_uppercase + string.digits, k=N))
        
        s = RandString(5)
        token = {"token" : s, "time": time}
        self.db.append(token)        
        with open(self.path, "wb") as f:
            f.write(json.dumps(self.db).encode("ascii"))
        
        return token

    