import paho.mqtt.client as mqtt
import db , json, random, string
import threading

def SetInterval(func, sec):
    def functionWrapper():
        SetInterval(func, sec)
        func()
    t = threading.Timer(sec, functionWrapper)
    t.start()
    return t



class ServerAction(object):
    
    def __init__(self):
        self.ip = "35.240.207.36"
        self.port = 1883
        self.mqtt = mqtt.Client("OP Warnet")
        self.mqtt.connect(self.ip, self.port)
        self.mqtt.subscribe("#")
        self.mqtt.loop_start() 
        self.clients = [] 
        SetInterval(self.UpdateTime, 1)   

        def OnMessage(client, userdata, message):
            
            data = json.loads(message.payload.decode("ascii"))
            if data["command"] == "LOGON":
                self.Verify(data["pc"], data["token"])
            if data["command"] == "CHECK":
                self.Verify(data["pc"], None)                
            if data["command"] == "MSG":
                print("login")
            if data["command"] == "MSG":
                print("login")
            if data["command"] == "BEAT":
                self.Beat(data["pc"])

            print('on message: {}'.format(message.payload))

        def OnPublish(client, userdata, mid):
            print("data to publish: " + str(mid))

        def OnConnect(client, userdata, flags, rc):
            print("rc: " + str(rc))

        self.mqtt.on_connect = OnConnect
        self.mqtt.on_message = OnMessage
        self.mqtt.on_publish = OnPublish

        self.db = db.Database()

    def SendChat(self, pc, message):
        msg = { "message": message }
        self.mqtt.publish(pc, json.dumps(msg))


    #fungsi untuk melihat list client
    def isClientInList(self, pc):
        res = [item for item in self.clients  if item.get('pc')==pc]
        if(len(res) > 0): return res[0]
        return None

    def Verify(self, pc, token):
        msg = {"command": "LOGON", "result" : "FAILED"}
        thisPc = self.isClientInList(pc)
        if(thisPc and token is None):
            msg = {"command": "CHECK", "result" : "LOGGED IN ARLEADY", "time": float((thisPc["time"])/3600)}
            self.mqtt.publish(pc, json.dumps(msg))
            return
        
        abc = self.db.VerifyToken(token)
        
        if(abc):
            msg["result"] = "SUCCESS"
            msg["time"] = abc["time"]
            self.mqtt.publish(pc, json.dumps(msg))
            self.clients.append({ "pc": pc, "time": float(msg["time"])*3600 })
            


            return
        self.mqtt.publish(pc, json.dumps(msg))

    def Beat(self, pc):
        msg = {"command": "BEAT", "action" : "none", "pc": pc}
        self.mqtt.publish(pc, json.dumps(msg))

    def UpdateTime(self):
        for i in range(len(self.clients)):
            if(self.clients[i]["time"] >  0): 
                self.clients[i]["time"] -= 1            
        #print(self.clients)

    def Kick(self, pc):
        if self.isClientInList(pc):
           msg = { "action": "KICK" }
           self.mqtt.publish(pc, json.dumps(msg))
           self.clients = [item for item in self.clients if item["pc"] != pc]

    def Extend(self, pc, time):
        if self.isClientInList(pc):
           msg = { "action": "EXTEND", "time": time }
           self.mqtt.publish(pc, json.dumps(msg))
           for i in range(len(self.clients)):
              if self.clients[i]["pc"] == pc:
                 self.clients[i]["time"] += float(time)*3600

    def GenerateKeyForPC(self, time):

        tok = self.db.GenerateToken(time)
        print("Generated string ", tok)
        #db here