import paho.mqtt.client as mqttClient
import random, string, json


HOST_NAME = "35.240.207.36"
PORT_NUMBER = 1883

time = ""

data = ()

def RandString(N):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=N))

def OnMessage(client, userdata, message):
    global data 
    try:
        data = json.loads(message.payload.decode("ascii"))
        print(data)
    except:
        data = message.payload
        print("invalid json")
    print('on message: {}'.format(message.payload))

def OnPublish(client, userdata, mid):
    print("data to publish: " + str(mid))

def OnConnect(client, userdata, flags, rc):
    print("rc: " + str(rc))

pc = RandString(3)
mqtt = mqttClient.Client(pc)
mqtt.on_connect = OnConnect
mqtt.on_message = OnMessage
mqtt.on_publish = OnPublish
mqtt.connect(HOST_NAME, PORT_NUMBER)
mqtt.subscribe(pc)
mqtt.loop_start()



def Logon(key):
    data = {
        "command": "LOGON",
        "pc": pc,
        "token": key
    }
    #r = requests.post(self.host+"/logon", json=data)        
    mqtt.publish(pc, json.dumps(data))
    print(data)

def Check(pc):
    data = {
        "command": "CHECK",
        "pc": pc,
    }
    #r = requests.post(self.host+"/logon", json=data)        
    mqtt.publish(pc, json.dumps(data))    
    print(pc)

def Beat(pc):
    data = {
        "command": "BEAT",
        "pc": pc,
    }
    
    #r = requests.post(self.host+"/logon", json=data)        
    mqtt.publish(pc, json.dumps(data))    
    print(pc)

def sendText(pc, msg):
    data = {
        "command": "CHAT",
        "pc": pc,
        "msg": msg
    }
    mqtt.publish(pc, json.dumps(data))    

    

def uploadFile(filename, msg):
    print(filename, " ", msg)
