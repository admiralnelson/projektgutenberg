import time
import requests
import os.path
import mimetypes
import action
import json
from http.server import BaseHTTPRequestHandler, HTTPServer

HOST_NAME = 'localhost'
PORT_NUMBER = 9005  

class WebUserInterface(BaseHTTPRequestHandler):    
    def do_HEAD(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        paths = ["/index", "/logon" , "/logout"]        
        if self.path in paths:
            self.respond(self.path)
        elif self.path.startswith("/res"):
            self.rawFile(self.path)            
        else:
            self.respond(self.path)

    def do_POST(self):
        paths = ["/index", "/logon", "/logout",  "/check", "/check", "/beat", "/say"]
        contentLen = int(self.headers.get('Content-Length'))
        postBody = self.rfile.read(contentLen).decode("UTF-8")
    
        if self.path in paths:
            self.respond(self.path, rest=True, bodyRequest=postBody)        
        else:
            self.respond(self.path)

    def indexPage(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        content = """
            <html><head><title>Title goes here.</title></head>
            <body>Welcome to Index page
            </body></html>
            """
        self.wfile.write(bytes(content, 'UTF-8'))
        return True

    def indexPage_REST(self, requestBody):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')        
        self.end_headers()
        
        requestBody = "Please wait..."
        self.wfile.write(bytes(requestBody, 'UTF-8'))
        return True

    def logonResult_REST(self, requestBody):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')        
        self.end_headers()

        content = json.loads(requestBody)
        action.Logon(content["logon"])    
        time.sleep(4)
        data = json.dumps(action.data)  
        self.wfile.write(bytes(data, 'UTF-8'))
        action.data = {}
        return True

    def check_REST(self, requestBody):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')        
        self.end_headers()

        #content = json.loads(requestBody)
        action.Check(action.pc)        
        time.sleep(4)
        data = json.dumps(action.data)           
        self.wfile.write(bytes(data, 'UTF-8'))
        action.data = {}
        return True

    def say_REST(self, requestBody):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')        
        self.end_headers()
        #print(requestBody)
        content = json.loads(requestBody)
        action.sendText(action.pc, content["say"])        
        #data = json.dumps(action.data)           
        self.wfile.write(bytes(requestBody, 'UTF-8'))
        return True


    def beat_REST(self, requestBody):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')        
        self.end_headers()

        #content = json.loads(requestBody)
        #action.Beat(action.pc)        
        data = json.dumps(action.data)          

        self.wfile.write(bytes(data, 'UTF-8'))

        return True

    def rawFile(self, route):
        route = route[5:]
        path = "res/"+route
        if os.path.isfile(path) :
            self.send_response(200)
            mime = mimetypes.MimeTypes().guess_type(path)[0]
            self.send_header('Content-type', mime)
            self.end_headers()
            with open(path, "rb") as f:            
                self.wfile.write(f.read())
        else:
            self.send_response(404)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(bytes("not found", 'UTF-8'))

    def respond(self, route, rest=False, bodyRequest=""):
        result = False

        if rest:
            if route == "/logon":
                result = self.logonResult_REST(bodyRequest)
            if route == "/check":
                result = self.check_REST(bodyRequest)
            if route == "/beat":
                result = self.beat_REST(bodyRequest)
            if route == "/say":
                result = self.say_REST(bodyRequest)

        else:
            if route == "/index":
                result = self.indexPage()
            if route == "/logon":
                result = self.indexPage()

        if not result:
            self.send_response(404, "Not found")
            self.end_headers()
            self.wfile.write(bytes("Not found", 'UTF-8'))
            

if __name__ == '__main__':
    server_class = HTTPServer
    httpd = server_class((HOST_NAME, PORT_NUMBER), WebUserInterface)    
    print(time.asctime(), 'Server Starts - %s:%s' % (HOST_NAME, PORT_NUMBER))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print(time.asctime(), 'Server Stops - %s:%s' % (HOST_NAME, PORT_NUMBER))