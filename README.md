# Kelompok Projektgutenbeg
1.  I Wayan Krishnadi Bima Widyastana (1301164014)
2.  Muhammad Jehan Mahsan (1301160441)
3.  Muhammad Zaki Faizal (1301160186)
4.  Andri Amirul Sonjaya (1301164234)

# Penjelasan Aplikasi  
Program untuk Warnet. [Cara kerja dan stack protokol yang digunakan](https://gitlab.com/admiralnelson/projektgutenberg/wikis/cara-kerja)  
[Dokumentasi fitur](https://gitlab.com/admiralnelson/projektgutenberg/wikis/Warnet)  

# Fitur Tubes Awal
1. Generate Code
2. Login
3. Logout
4. Set Timer

# Fitur Cots
1. Chat satu arah dari client ke server -- (I Wayan Krishnadi Bima Widyastana--1301164014)
2. Melihat list pc di server -- (Andri Amirul Sonjaya--1301164234)
3. Menambahkan timer yang sudah login pada client -- (Muhammad Zaki Faizal--1301160186)
4. Kick Client pada Server -- (Muhammad Jehan Mahsan--1301160441)

# Requirement
1. Python 3
2. Paho-mqtt package
3. web browser

